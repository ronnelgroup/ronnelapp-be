'use strict';

const server = require('./index'); 
const port = process.env.PORT;
console.log("your port is: ", port);
const connectDB = require('./src/database/db');
const { MONGO_URI, MONGO_DEV, NODE_ENV, MONGO_TEST } = process.env;
let MONGO_URL = MONGO_DEV;
switch (NODE_ENV) {
  case 'production':
    MONGO_URL = MONGO_URI;
    break;
  case 'development':
    MONGO_URL = MONGO_DEV;
    break;
  default:
    MONGO_URL = MONGO_TEST;
    break;
} 
connectDB(MONGO_URL);
server.listen(port, () => {
  console.log(`This app listening at http://127.0.0.1:${port}`);
});
