const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment-timezone');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const UserSchema = new Schema({
  UserId: {
    type: String,
    required: false,
  },
  UserName: {
    type: String,
    required: true,
    unique: true,
  },
  Password: {
    type: String,
    required: true,
    minLength: 6
  },
  UserToken: {
    type: String,
    default: null,
  },  
  Role: {
    type: String,
    required: true,
    enum: ['Waiter', 'Cashier', 'Kitchen', 'Admin', 'Manager'],
  },
  IsActive: {
    type: Boolean,
    required: false,
    default: true,
  },
  DateCreated: {
    type: Date,
    default: moment().tz('Asia/Manila').format(),
  },
  UpdatedAt: {
    type: Date,
    default: null,
  },
});

UserSchema.pre(
  [
    'update',
    'updateOne',
    'updateMany',
    'findOneAndUpdate',
    'findByIdAndUpdate',
  ],
  async function (next) {
    this._update.UpdatedAt = moment().tz('Asia/Manila').format();
    this._update.Token = null
    if (this._update.Password) {
      const salt = await bcrypt.genSalt();
      this._update.Password = await bcrypt.hash(this._update.Password, salt);
    }
    next();
  }
);

UserSchema.pre('save', async function (next) {
  const salt = await bcrypt.genSalt();
  this.Password = await bcrypt.hash(this.Password, salt);
  next();
});

UserSchema.post('save', async function (doc, next) {
  delete doc._doc.Password;
  next();
});

UserSchema.methods.generateAuthToken = function () {
  delete this._doc.Password;
  delete this._doc.Token;
  const expiration = process.env.JWT_EXPIRATION || '12h';
  const secret = process.env.JWT_SECRET || 'ronnel-jwt-secret-key';
  const token = jwt.sign(this._doc, secret, { expiresIn: expiration });
  return token;
};

UserSchema.methods.isValidPassword = function (password) {
  const user = this;
  const compare = bcrypt.compareSync(password, user.Password);
  return compare;
};

const Users = mongoose.model('tbl_users', UserSchema);

module.exports = Users;
