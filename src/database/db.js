var mongoose = require('mongoose');
const connectDB = async (MONGO_URL) => {
  try {
    //mongodb connection string
    const con = await mongoose.connect(MONGO_URL);

    console.log(`MongoDB connected : ${con.connection.host}`);
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

module.exports = connectDB;
