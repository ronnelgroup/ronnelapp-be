const { validationResult } = require('express-validator');
exports.successResponse = (data) => {
  return {
    mess: 'Successful', 
    status: 'success',
    data: data,
  };
};
exports.serverErrorResponse = () => {
  return {
    mess: 'Internal Server Error', 
    status: 'fail',
    data: '',
  };
};

exports.validationErrorResponse = (req) => {
  const errors = validationResult(req);
  const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
    // Build your resulting errors however you want! String, object, whatever - it works!
    return { [param]: msg };
  };
  return {
    mess: 'Failed', 
    status: 'fail',
    data: errors.formatWith(errorFormatter).array(),
  };
};

exports.noDataFoundResponse = () => {
  return {
    mess: 'No Data found', 
    status: 'fail',
    data: [],
  };
};
 
