const { validationResult, matchedData, check } = require('express-validator');

const validate = (validations) => {
  return async (req, res, next) => {
    await Promise.all(validations.map((validation) => validation.run(req)));

    const errors = validationResult(req);
    if (errors.isEmpty()) {
      const data = matchedData(req);
      req.data = data;
      return next();
    }

    const errorObj = {};
    const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
      errorObj[param] = msg;
      return { [param]: msg };
    };
    errors.formatWith(errorFormatter).array();
    return res.status(422).json({
      mess: 'Failed', 
      status: 'fail',
      data: errorObj,
    });
  };
};

module.exports = { validate, check };
