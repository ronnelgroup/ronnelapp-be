'use strict';

const _ = require('lodash');
const { validate, check } = require('../validationHandler');
const User = require('../../models/cmsUserModel'); 
const { UserAccess, UserRoles } = require('../../helpers/constants');
const strictOptional = { nullable: true, checkFalsy: true };
module.exports = {
  listUsers: () => {
    return validate([
      check('page').optional(strictOptional).isInt().toInt(),
      check('limit').optional(strictOptional).isInt().toInt(),
      check('search').optional(strictOptional).isString().escape().trim(),
      check('isActive')
        .if(check('isActive').notEmpty({ ignore_whitespace: true }))
        .isBoolean()
        .toBoolean(),
    ]);
  },

  addUsers: () => {
    return validate([
      check('UserId')
        .optional(strictOptional)
        .trim()
        .isString()
        .withMessage('Not a valid Employee Id')
        .bail()
        .custom(async (UserId) => {
          const res = await User.getUserByUserId(UserId);
          return !_.isEmpty(res) ? Promise.reject('Employee ID already used or existing') : Promise.resolve(true);
        }),
      check('UserName')
        .trim()
        .notEmpty({ ignore_whitespace: true })
        .withMessage('Username is required')
        .bail()
        .isString()
        .bail()
        .custom(async (UserName) => {
          const res = await User.getUserByUsername(UserName);
          return !_.isEmpty(res) ? Promise.reject('Username is already taken') : true;
        }),
      check('Role', 'Role is required')
        .notEmpty({ ignore_whitespace: true })
        .bail()
        .isIn(UserRoles)
        .withMessage('Invalid role'),
      check('IsActive', 'Invalid status value').optional({ nullable: true }).isBoolean({ strict: true }),
    ]);
  },

  updateUserById: () => {
    return validate([
      check('Role', 'Invalid role value').optional(strictOptional).isIn(UserRoles),
      check('Password', 'Invalid password').optional(strictOptional).isString(),
      check('Password', 'Password must be atleast 6 characters').optional(strictOptional).isLength({
        min: 6,
      }),
      check('IsActive', 'Invalid status value').optional({ nullable: true }).isBoolean({ strict: true }),
    ]);
  },

  checkLogin: () => {
    return validate([
      check('UserName')
        .trim()
        .notEmpty({ ignore_whitespace: true })
        .withMessage('Username is Required')
        .bail()
        .isString(),
      check('Password', 'Password is Required').notEmpty({ ignore_whitespace: true }),
    ]);
  },
    
  checkId: () => {
    return validate([check('id', 'Invalid user id').isMongoId()]);
  },
};
