const jwt = require('jsonwebtoken');
const moment = require('moment-timezone');
const config = process.env;
const _ = require('lodash');
const User = require('../models/cmsUserModel');
const { UserRoles } = require('../helpers/constants'); 
exports.verifyToken = function (role = UserRoles, Access = '') {
  return async (req, res, next) => {
    try {
      const token = req.headers['x-auth-token'];
      if (!token) {
        return res.status(403).json({
          mess: 'No session found!', 
          status: 'fail',
          data: null,
        });
      }
      const payload = jwt.decode(token, config.JWT_SECRET);
      if (payload.exp < moment().unix()) {
        return res.status(401).json({
          mess: 'Session expired!', 
          status: 'fail',
          data: null,
        });
      }
      const user = await User.getUserByUserId(payload.UserId);
      if (!user) {
        return res.status(401).send({
          mess: 'Not valid token!', 
          status: 'fail',
          data: null,
        });
      } 
      if (role.includes(payload.Role)) { 
        req.user = payload; 
        return next();
      } else {
        return res.status(401).send({
          mess: 'Role is not allowed!', 
          status: 'fail',
          data: null,
        });
      }
    } catch (err) {
      return res.status(401).send({
        mess: 'Not valid token!', 
        status: 'fail',
        data: null,
      });
    }
  };
};

exports.guest = (req, res, next) => {
  const token = req.headers['x-auth-token'] || req.headers['authorization'];

  if (!_.isEmpty(token)) {
    return res.status(401).json({
      mess: 'Session found', 
      status: 'fail',
      data: null,
    });
  }

  next();
};
 
