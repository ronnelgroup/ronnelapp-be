'use-strict';

const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const UserModel = require('../schema/UserSchema');
const _ = require('lodash');
const { metadata, paginate } = require('../helpers/constants');
module.exports = {
  async getAllUserList({ page, limit, search, isActive }) { //********* */
    page = page ? parseInt(page) : 1;
    limit = limit ? parseInt(limit) : 10;
    // all = all ? { BranchCode: { $eq: branchCode } } : {};
    isActive = typeof isActive === 'boolean' ? { IsActive: isActive } : {};
    search = search ? search : '';
    // search = search
    //   ? { 'UserName': { $regex: '.*' + search + '.*' } }
    //   : {};
    const pagination = paginate(page, limit);
    const sort = { $sort: { CreatedAt: -1 } };
    let aggs = [
      {
        $match: {
          $and: [
            {
              $or: [
                {
                  UserName: {
                    $regex: `.*${search}.*`,
                  },
                },
              ],
            },
            isActive,
          ],
        },
      },
    ];
    const paginateQuery = _.union(aggs, metadata);
    aggs.push(...pagination, sort);
    return new Promise(async (resolve, reject) => {
      try {
        const res = await UserModel.aggregate([
          {
            $facet: {
              paginates: aggs,
              metadata: paginateQuery,
            },
          },
        ]).allowDiskUse(true);
        return resolve({
          paginatedResults: res[0].paginates,
          metadata: {
            page: page,
            limit: limit,
            total: res[0].metadata.length ? res[0].metadata[0].total : 0,
          },
        });
      } catch (err) {
        return reject(err);
      }
    });
  },

  async addUsers(dataBody = {}) { //********* */
    let message = '';
    const addedUsers = await new Promise((resolve, reject) => {
      const Users = new UserModel(dataBody);
      Users.save(function (err, res) {
        if (err) {
          message = { Error_code: 1, Data: err };
          return reject(message);
        } else {
          message = { Error_code: 0, Data: res };
          return resolve(message);
        }
      });
    });
    return addedUsers;
  },
 

  async getAllUsers() {
    const Users = await new Promise((resolve, reject) => {
      UserModel.find()
        .then((users) => {
          return resolve(users);
        })
        .catch((err) => {
          return reject(err);
        });
    });
    return Users;
  },

  async updateUserId(id = ObjectId, userBody) { //********* */
    return await new Promise((resolve, reject) => {
      console.log(id)
      console.log(userBody)
      let message = {};
      
      UserModel.findByIdAndUpdate(id, userBody, {
        upsert: true,
        new: true,
      })
        .then((users) => {
          if (!users) {
            message = { Error_code: 1, Data: users };
            console.log(message)
            return resolve(message);
          }
          message = { Error_code: 0, Data: users };
          console.log(message)
          return resolve(message);
        })
        .catch((err) => {
          console.log(err)
          return reject(err);
        });
    });
  }, 

  async deleteUsersById(id) { //********* */
    const deletedUsers = await new Promise((resolve, reject) => {
      let message = {};
      UserModel.findByIdAndDelete(id)
        .then((users) => {
          if (!users) {
            message = { Error_code: 1, Data: users };
            return resolve(message);
          }
          message = { Error_code: 0, Data: users };
          return resolve(message);
        })
        .catch((err) => {
          return reject(err);
        });
    });
    return deletedUsers;
  },

  getUserByUserId(id) {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await UserModel.findOne({
          UserId: id,
        });
        return resolve(res);
      } catch (error) {
        return reject(error);
      }
    });
  },

  getUserById(id) { //********* */
    return new Promise(async (resolve, reject) => {
      try {
        const res = await UserModel.findById(id);
        return resolve(res);
      } catch (error) {
        return reject(error);
      }
    });
  },

  getUserByUsername(username = '') {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await UserModel.findOne({
          UserName: username,
        });
        return resolve(res);
      } catch (error) {
        return reject(error);
      }
    });
  },

  getUserDetail(username = '') {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await UserModel.find({
          UserName: username,
        });
        return resolve(res);
      } catch (error) {
        return reject(error);
      }
    });
  },

  async updateToken(userBody) { //********* */
    return await new Promise((resolve, reject) => { 
      let message = {};
      let id = userBody.id
      let LoginToken = userBody.Token 
      console.log(LoginToken)
      UserModel.updateOne( 
        { _id: id },
        { $set: { UserToken: LoginToken } },
        (err, res) => {
          if (err) {
            message = { Error_code: 2, Data: err };
            return reject(message);
          } 
          if (!res) {
            message = { Error_code: 1, Data: res }; 
            return resolve(message);
          } 

          message = { Error_code: 0, Data: res };
          console.log(message)
          return resolve(message);
        }
      );
    });
  },
};


