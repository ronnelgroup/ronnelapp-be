'use strict';
const jwt = require('jsonwebtoken');
const moment = require('moment-timezone');
const _ = require('lodash');
const UserMod = require('../models/cmsUserModel');
const posUserLog = require('../schema/UserSchema');
const clientResponse = require('../middlewares/clientResponse');
const success = require('../../public/language/success.json');
const asyncHandler = require('../middlewares/asyncHandler');
const { UserAccess, UserRoles } = require('../helpers/constants');
const { UserErrors } = require('../../public/language/errors.json');
const { generalErrors } = require('../../public/language/errors.json');

exports.userAllList = asyncHandler(async (req, res) => { 
  const query = req.data;
  const user = await UserMod.getAllUserList(query);

  if (_.isEmpty(user.paginatedResults)) {
    if (query.search) {
      return res.status(generalErrors.searchFail.code).json(generalErrors.searchFail.messages);
    }
    return res.status(generalErrors.noResourceFound.code).json(generalErrors.noResourceFound.messages);
  }
  return res.status(200).json(clientResponse.successResponse(user));
});

exports.userInfo = asyncHandler(async (req, res) => { //********* */
  const { id } = req.data;
  const users = await UserMod.getUserById(id);
  if (!users) {
    return res.status(404).json(clientResponse.noDataFoundResponse());
  }
  return res.status(200).json(clientResponse.successResponse(users));
});

exports.addUsers = asyncHandler(async (req, res) => {   //********* */
  const body = req.data;
  const password = (Math.random() + 1).toString(36).substring(2).toUpperCase();
  body.Password = password;
  const users = await UserMod.addUsers(body);
  if (users.Error_code == 1) {
    return res.status(UserErrors.UserCreateError.code).json(UserErrors.UserCreateError.messages);
  }
  return res.status(201).json(clientResponse.successResponse({ password }));
});
  
exports.updateUser = asyncHandler(async (req, res) => { //********* */
  const { id } = req.data;
  const userBody = req.data;
  delete userBody.id;
  console.log(userBody)
  console.log("UserID: ", id)
  const Users = await UserMod.updateUserId(id, userBody);
  if (!Users) {
    return res.status(404).json(clientResponse.noDataFoundResponse());
  }
  return res.status(202).json(clientResponse.successResponse(Users));
});

exports.deleteUserById = asyncHandler(async (req, res) => { //********* */
  const { id } = req.data;
  const Users = await UserMod.deleteUsersById(id);
  if (Users.Error_code == 1) {
    return res.status(404).json(clientResponse.noDataFoundResponse());
  }
  return res.status(success.delete.code).json(success.delete.messages);
});

exports.loginUser1 = function (req, res) { //********* */
  // find the user in database
  posUserLog.findOne(
    {
      UserName: req.data.UserName,
    },
    async function (err, user) {
      //If there is any error is finding or doing operation in database 
      if (err) {
        return res.status(500).json(clientResponse.serverErrorResponse());
      }
      //if does not find username in database then return error
      if (!user) {
        return res.status(UserErrors.InvalidCredentials.code).json(UserErrors.InvalidCredentials.messages);
      }

      let bool = user.isValidPassword(req.body.Password);

      if (!bool) {
        console.log('here');
        return res.status(UserErrors.InvalidCredentials.code).json(UserErrors.InvalidCredentials.messages);
      }
      //check if User has existing token
      if (user.Token) {
        const payload = jwt.decode(user.Token, process.env.JWT_SECRET);
        //verify if token is not yet expired
        if (payload.exp > moment().unix()) {
          return res.status(200).json(clientResponse.successResponse(user.Token));
        }
      } 
      const token = user.generateAuthToken();  
      return res.status(200).json(clientResponse.successResponse(token));
    }
  );
};

exports.loginUser = asyncHandler(async (req, res) => { 
  const body = req.data;
  const UserName = body.UserName
  const UsersDetl = await UserMod.getUserDetail(UserName);
  let Users = UsersDetl[0]
  console.log(Users)
  if(Users == null){
    return res.status(UserErrors.InvalidCredentials.code).json(UserErrors.InvalidCredentials.messages);
  }
  let bool = Users.isValidPassword(req.body.Password);
  if (!bool) {
    console.log('here');
    return res.status(UserErrors.InvalidCredentials.code).json(UserErrors.InvalidCredentials.messages);
  }

  if (Users.UserToken) {
    const payload = jwt.decode(Users.UserToken, process.env.JWT_SECRET);
    //verify if token is not yet expired
    if (payload.exp > moment().unix()) {
      return res.status(200).json(clientResponse.successResponse(Users.UserToken));
    }
  } 
  const token = Users.generateAuthToken();  

  const userBody = {
    id: Users._id,
    Token: token
  };

  const UserUpdate = await UserMod.updateToken(userBody);
  return res.status(200).json(clientResponse.successResponse(token));
  
});
