'use strict';

module.exports = {
  metadata: [
    {
      $group: {
        _id: null,
        total: { $sum: 1 },
      },
    },
  ],
  paginate(page, limit) {
    return [{ $skip: (page - 1) * limit }, { $limit: limit }];
  }, 
  UserRoles: ['Waiter', 'Cashier', 'Kitchen', 'Admin', 'Manager']
};
