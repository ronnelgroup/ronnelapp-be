const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const dotenv = require('dotenv');
const compress = require('compression');
const helmet = require('helmet');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

const app = express();
const server = require('http').createServer(app);


app.use(cors());
require('dotenv').config();
const port = process.env.PORT
//Middleware
const adminJWT = require('./src/middlewares/tokenVerification');
//config
function customHeaders(req, res, next) {
  // Switch off the default 'X-Powered-By: Express' header
  app.disable('x-powered-by');
  app.disable('server');
  // OR set your own header here
  res.setHeader('X-Powered-By', 'Ronnel-API v0.0.1');
  next();
}
app.use(customHeaders);
app.use(compress());
app.use(helmet());
const corsOption = {
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['x-auth-token'],
};
app.use(cors(corsOption));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));
app.use(function (req, res, next) {
  res.locals.url = req.protocol + '://' + req.headers.host;
  next();
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
//CMS
app.use('/cms/users', require('./routes/cmsUsers'));

// catch 404 and forward to error handler
app.use(function(err, req, res, next) {
  // next(createError(404));
  res.status(404);
  let message = {
    mess: 'Page not found',
    messVn: 'Không tìm thấy tài nguyên',
    status: 'fail',
    data: err,
  };
  res.send(message);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
}); 
 
module.exports = server;
