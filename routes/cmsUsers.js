'use strict';

const express = require('express');
const router = express.Router();
const cmsUsersController = require('../src/controllers/cmsUsersController');

const auth = require('../src/middlewares/tokenVerification');
const validate = require('../src/middlewares/validation/cmsUserManagementValidation');
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

router.post('/add', auth.verifyToken(['Admin']), validate.addUsers(), cmsUsersController.addUsers);
router.get('/list', auth.verifyToken(), validate.listUsers(), cmsUsersController.userAllList);
router.get('/details/:id', auth.verifyToken(), validate.checkId(), cmsUsersController.userInfo);
router.put('/update/:id', auth.verifyToken(['Admin']), validate.checkId(), validate.updateUserById(), cmsUsersController.updateUser); 
router.delete('/delete/:id', auth.verifyToken(['Admin']), validate.checkId(), cmsUsersController.deleteUserById);
 
router.post('/login', auth.guest, validate.checkLogin(), cmsUsersController.loginUser);

module.exports = router;
