# RONNELAPP BE

## Commit Changes Logs

0.0.8:

- Fix Saving User Login Token 

0.0.7:

- Setup Installation and Workaround Guides on README.md

0.0.6:

- Delete User, Fix Role validation

0.0.5:

- Fetch All Users, Fetch user information by ID, Update User

0.0.4:

- Setup Dependencies, Create New user, Login User

0.0.3:

- Setup Mongodb Database Connection

0.0.2:

- Create Modular Structure Codes

0.0.1:

- Initialized Commit

*** 
## Steps how to clone this repository
- [NOTE] In order to be able clone this repository you need to put you ssh keygen first into: 
    - Preferences > SSH KEYS Page
    - Then Add your own pc ssh-keygen into "Key" Texbox and then 
    - then click Add Key Button 
- Then on your git bash or any git tools preferred: 

```
git clone git@gitlab.com:ronnelgroup/ronnelapp-be.git (To clone the repo)
cd ronnelapp-be (Locate Inside folder project)
npm install (To install node_modules dependencies)
npm run dev (To run the app) 
```

*** 
## Work Around on Postman
- Get token first from login admin results then paste in on Environments > local > INITIAL VALUE and CURRENT VALUE of admin-token then save it. See Screenshot below:
![Semantic description of image](/public/images/images1.JPG)
![Semantic description of image](/public/images/images2.JPG)

- Then select local environment first on top right of postman panel to use the auth for add,update,read and delete user.
![Semantic description of image](/public/images/images3.JPG)